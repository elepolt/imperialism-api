from rest_framework.permissions import IsAuthenticatedOrReadOnly
from django.contrib.auth.models import User
import maps.models as models
import maps.serializers as serializers

from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

import datetime 

# @api_view(['GET')
class RedditPostViewSet(viewsets.ModelViewSet):
	"""
	API endpoint that allows BlogPosts to be viewed or edited.
	"""

	def get_queryset(self):
			""" allow rest api to filter by submissions """
			queryset = models.RedditPost.objects.order_by('-reddit_create_date')
			if 'post_type' in self.kwargs:
				post_type = self.kwargs['post_type']# self.request.query_params.get('post_type', None)
				if post_type is not None:
					queryset = queryset.filter(post_type=post_type).order_by('-reddit_create_date')
			
			return queryset
	queryset = models.RedditPost.objects.order_by('-reddit_create_date')
	serializer_class = serializers.RedditPostSerializer
	permission_classes = (IsAuthenticatedOrReadOnly,)

class YearsList(APIView):
	"""
	API endpoint that allows BlogPosts to be viewed or edited.
	"""
	def get(self, request, format=None):
		# Gets unique years
		years = models.RedditPost.objects.all().values('reddit_create_date__year').distinct()
		years_query = []
		for year in years:
			# Loop through years and get the first and last post from that season
			season_year = year['reddit_create_date__year']
			season_start = datetime.date(season_year, 10, 1)
			season_end = datetime.date(season_year+1, 5, 5)
			year_query = models.RedditPost.objects.filter(reddit_create_date__gte=season_start).filter(reddit_create_date__lte=season_end)
			years_query.append(year_query.first())
			years_query.append(year_query.last())

		serializer = serializers.RedditPostSerializer(years_query, many=True)
		return Response(serializer.data)