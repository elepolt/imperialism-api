from rest_framework import serializers
from maps.models import RedditPost


class RedditPostSerializer(serializers.ModelSerializer):
  class Meta:
    model = RedditPost
    fields = ['id', 'created','title','reddit_id','reddit_create_date','year','body','post_type','image_url','thumbnail_url','permalink']