from django.db import models

# Create your models here.
class RedditPost(models.Model):
  created = models.DateTimeField(auto_now_add=True)
  title = models.CharField(max_length=150, blank=True, default='')
  reddit_id = models.CharField(max_length=150, blank=False, default='', unique=True)
  reddit_create_date = models.DateTimeField()
  body = models.TextField()
  image_url = models.CharField(max_length=150, blank=True, default='')
  thumbnail_url = models.CharField(max_length=150, blank=True, default='')
  permalink = models.CharField(max_length=150, blank=True, default='')
  post_type = models.CharField(max_length=15, blank=True, default='')

  def year(self):
    return self.reddit_create_date.year
  def __str__(self):
    return self.title