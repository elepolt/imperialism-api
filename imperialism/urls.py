from django.urls import include, path
from rest_framework import routers
from maps import views
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns

router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
router.register(r'reddit-posts/(?P<post_type>.+)', views.RedditPostViewSet)
router.register(r'reddit-posts', views.RedditPostViewSet)
# router.register(r'years/<int:year>', views.years_list)
# router.register(r'reddit-posts/<int:year>', views.RedditPostViewSet)
# router.register(r'years', views.YearsViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    path('years/', views.YearsList.as_view()),

]
